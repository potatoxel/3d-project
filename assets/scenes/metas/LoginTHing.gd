extends Panel

func _ready():
	Networking.connect("connection_success", self, "_on_connection_success")
	

func _on_join_pressed():
	Networking.join_server($ip.text, $username.text)


func _on_host_pressed():
	Networking.host_server($username.text)

func _on_connection_success(success):
	get_tree().change_scene("res://assets/scenes/demos/demo_1.tscn")
