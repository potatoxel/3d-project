extends Spatial

const Player = preload("res://assets/scenes/subscenes/character/character.tscn")

var player

func _ready():
	$NetworkWorld.connect("world_loaded", self, "_on_world_load")

func _on_world_load():
	player = Player.instance()
	player.set_network_master(Networking.my_id)
	player.name = str(Networking.my_id) + "_player"
	add_child(player)
	player.global_transform.origin = Vector3(0, 1, 0)
	$NetworkWorld.add_entity(player.get_node("NetworkEntity"))
	

