##########################################################################
#   Copyright (C) 2021 Abdullah Çırağ
#   This file is part of 3D Project.
#   3D Project is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#   3D Project is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
##########################################################################
extends "res://assets/scripts/mover.gd"
#ABSTRACT, DO NOT ATTACH

func _physics_process(delta):
	_gravitate(delta)

func _gravitate(delta):
	if(self.has_method("get_is_dashing")): #If character
		if (self.call("get_is_dashing")):
			pass
		else:
			_add_to_velocity(max(0.0, (Global._gravity) * delta) * Vector3.DOWN)
	else:
		_add_to_velocity(max(0.0, (Global._gravity) * delta) * Vector3.DOWN)
# + _velocity.y
