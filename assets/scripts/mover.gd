##########################################################################
#   Copyright (C) 2021 Abdullah Çırağ
#   This file is part of 3D Project.
#   3D Project is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#   3D Project is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
##########################################################################
class_name KinematicBodyThatCanEasilyRub
extends KinematicBody
#ABSTRACT, DO NOT ATTACH

var _acceleration
var _velocity = Vector3()
var _friction = 0.0

func _physics_process(delta):
	_rub(delta)
	_velocity = move_and_slide(_velocity, Vector3.UP)

func _set_velocity(var value):
	if (typeof(value) == TYPE_VECTOR3):
		_velocity = value
	else:
		print("Exception: Set value is not a Vector3.") #Improve this

func _add_to_velocity(var value):
	if (typeof(value) == TYPE_VECTOR3):
		_velocity += value
	else:
		print("Exception: Added value is not a Vector3.") #Improve this

func _rub(delta):
	if(is_on_floor()):
		_velocity -= _friction * _velocity * delta
	else:
		_velocity -= _friction / 6.0 * _velocity * delta
	if (Vector3(_velocity.x, 0.0, _velocity.z).length_squared() < 0.01):
		_velocity -= Vector3(_velocity.x, 0.0, _velocity.z)
