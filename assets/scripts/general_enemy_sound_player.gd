##########################################################################
#   Copyright (C) 2021 Abdullah Çırağ
#   This file is part of 3D Project.
#   3D Project is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#   3D Project is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
##########################################################################
extends AudioStreamPlayer

const EXISTENCE_DURATION = 1.5

enum Sounds {SOUND_HURT, SOUND_DEATH}

var existence_timer : float
var _enemy_hurt = preload("res://assets/sounds/enemy_hurt.ogg")
var _enemy_death = preload("res://assets/sounds/enemy_death.ogg")

func _ready():
    existence_timer = EXISTENCE_DURATION
    bus = "SFX"

func _process(delta):
    _run_existence_timer(delta)

func play_sound(var sound):
    match sound:
        Sounds.SOUND_HURT:
            stream = _enemy_hurt
        Sounds.SOUND_DEATH:
            stream = _enemy_death
        _:
            print("Error: Supposed to be Sounds enum.")
    play()

func _run_existence_timer(delta):
    if (existence_timer > 0.0):
        existence_timer -= delta
    else:
        queue_free()
