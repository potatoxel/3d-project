##########################################################################
#   Copyright (C) 2021 Abdullah Çırağ
#   This file is part of 3D Project.
#   3D Project is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#   3D Project is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
##########################################################################
extends "res://assets/scripts/jumper.gd"
#CROUCHING / SLIDING???
const ACCEPTABLE_WALL_JUMP_INITIATION_DURATION = 0.2
const DASH_COOLDOWN = 0.8
const SLASH_COOLDOWN = 0.3
const SLASH_INITIALISATION = 0.1
const INVULNERABILITY_COOLDOWN = 1.5

onready var _slash = preload("res://assets/scenes/subscenes/slash/slash.tscn")
onready var _dash_sound = preload("res://assets/scenes/subscenes/character/dash.ogg")
onready var _jump_sound = preload("res://assets/scenes/subscenes/character/jump.ogg")
onready var _jump_air_sound = preload("res://assets/scenes/subscenes/character/jump_air.ogg")
onready var _hurt_sound = preload("res://assets/scenes/subscenes/character/hurt.ogg")

onready var _character_camera = get_node("CharacterCamera")
onready var _jump_status_box_texture_rect = get_node("HUDCanvasLayer/JumpStatusBoxTextureRect")
onready var _dash_status_box_texture_rect = get_node("HUDCanvasLayer/DashStatusBoxTextureRect")
onready var _health_bar_texture_progress = get_node("HUDCanvasLayer/HealthBarTextureProgress")
onready var _character_arm = get_node("CharacterCamera/CharacterArm")
onready var _character_audio_stream_player = get_node("CharacterAudioStreamPlayer")

var _input_movement_right = false
var _input_movement_left = false
var _input_movement_forward = false
var _input_movement_backward = false
var _input_movement_jump = false
var _input_movement_dash = false
var _input_combat_slash = false

var _movement_input_vector = Vector2()
var _jump_count = 2
var _last_wall_jump_timer = 0.0
var _last_wall_collision_normal = Vector3()
var _is_dashing = false
var _dash_duration = 0.3
var _dash_duration_timer = 0.0
var _dash_velocity = 20.0
var _dash_cooldown_timer = 0.0
var _dash_count = 1
#var aim_vector = Vector3()
var dash_vector = Vector3()
var green_status_box_color = Color(0.5, 1.0, 0.5, 1.0)
var red_status_box_color = Color(1.0, 0.5, 0.5, 1.0)
var _slash_cooldown_timer = 0.0
var _slash_initialisation_timer = 0.0
var _is_slashing = false
var _health = 100.0
var _invlunerability_timer = 0.0

var colcount = 0

func _ready():
	_set_inherited_variables()
	
	set_process(is_network_master())
	set_physics_process(is_network_master())
	$CharacterCamera.current = is_network_master()

func _process(delta):
	_run_last_wall_jump_timer(delta)
	_run_dash_duration_timer(delta)
	_run_dash_cooldown_timer(delta)
	_run_invulnerability_timer(delta)
	_update_jump_status_box()
	_update_dash_status_box()
	_update_health_bar()
	_run_slash_cooldown_timer(delta)
	_run_slash_initialisation_timer(delta)
	

func _physics_process(delta):
	_get_movement_input()
	_reset_jump_count()
	_reset_dash_count()
	#Input
	_set_movement_vector()
	#Horizontal Movement
	_move_with_movement_input(delta)
	#Vertical Movement
	_update_wall_collision()
	_jump_with_movement_input()
	_dash_with_movement_input()
	_slash_with_combat_input()
	_collide_with_physicsbodys()
	if (OS.is_debug_build()):
		if (Input.is_action_just_pressed("debug_reset_position")):
			translation = Vector3(0.0, 0.9, 0.0)
		if (Input.is_action_pressed("debug_toggle_key")):
			print(Vector3(_velocity.x, 0.0, _velocity.z).length())

func get_is_dashing():
	return _is_dashing

func _get_movement_input():
	_input_movement_right = Input.is_action_pressed("character_right")
	_input_movement_left = Input.is_action_pressed("character_left")
	_input_movement_forward = Input.is_action_pressed("character_forward")
	_input_movement_backward = Input.is_action_pressed("character_backward")
	_input_movement_jump = Input.is_action_just_pressed("character_jump")
	_input_movement_dash = Input.is_action_just_pressed("character_dash")
	_input_combat_slash = Input.is_action_just_pressed("character_slash")

func _set_inherited_variables():
	_friction = 6.0
	_acceleration = 80.0
	_jump_strength = 12.0

func _run_last_wall_jump_timer(delta):
	if (_last_wall_jump_timer > 0.0):
		_last_wall_jump_timer -= delta

func _reset_jump_count():
	if (is_on_floor()):
		_jump_count = 2

func _set_movement_vector():
	var movement_x_axis = float(_input_movement_right) - float(_input_movement_left)
	var movement_z_axis = float(_input_movement_forward) - float(_input_movement_backward)
	_movement_input_vector = Vector2(movement_x_axis, movement_z_axis).normalized()

func _move_with_movement_input(delta):
	#Horizontal
	if(is_on_floor()):
		_add_to_velocity((_movement_input_vector.y * (-transform.basis.z) +
		_movement_input_vector.x * transform.basis.x) * _acceleration * delta)
	else:
		_add_to_velocity((_movement_input_vector.y * (-transform.basis.z)+
		_movement_input_vector.x * transform.basis.x) * _acceleration / 6.0 * delta)
	#Vertical

func _update_wall_collision():
	if (is_on_wall()):
		_last_wall_jump_timer = ACCEPTABLE_WALL_JUMP_INITIATION_DURATION
		_last_wall_collision_normal = get_slide_collision(get_slide_count() - 1).normal

func _wall_jump():
	_add_to_velocity(Vector3.DOWN * _velocity.y +
	(_jump_strength) * (2 * _last_wall_collision_normal + Vector3.UP).normalized())
	_character_audio_stream_player.set_stream(_jump_sound)
	_character_audio_stream_player.play()

func _jump_with_movement_input():
	if(_input_movement_jump):
		if (_last_wall_jump_timer > 0.0 && _dash_cooldown_timer <= 0.0):
				_wall_jump()
				_last_wall_jump_timer = 0.0
		else:
			if (_jump_count > 0 && !_is_dashing):
				_jump()
				_jump_count -= 1

func _move_linearly(delta, var value):
	if (typeof(value) == TYPE_VECTOR3):
		_velocity = value
	else:
		print("Exception: Added value is not a Vector3.") #Improve this

func _run_dash_duration_timer(delta):
	if (_dash_duration_timer > 0.0):
		_dash_duration_timer -= delta
		_is_dashing = true
		_move_linearly(delta, dash_vector * _dash_velocity)
	else:
		if (_is_dashing == true):
			_velocity /= 4.0
		_is_dashing = false

func _dash():
	dash_vector = -_character_camera.global_transform.basis.z
	if (dash_vector.y > 0.0):
		dash_vector.y = 0.0
	_velocity = Vector3()
	_dash_duration_timer = _dash_duration
	_dash_cooldown_timer = DASH_COOLDOWN
	_dash_count -= 1
	_character_audio_stream_player.set_stream(_dash_sound)
	_character_audio_stream_player.play()


func _run_dash_cooldown_timer(delta):
	if (_dash_cooldown_timer > 0.0):
		_dash_cooldown_timer -= delta

func _dash_with_movement_input():
	if(_input_movement_dash && _dash_cooldown_timer <= 0.0 && _dash_count > 0):
		_dash()

func _reset_dash_count():
	if (is_on_floor() || is_on_wall()):
		_dash_count = 1

func _on_AreaDetector_area_entered(var area):
	if(area.is_in_group("teleporter") && area.has_method("get_teleport_destination")):
		transform.origin = area.get_teleport_destination()

func _update_jump_status_box():
	if (_jump_count > 0 && !_is_dashing):
		if (_jump_status_box_texture_rect.modulate != green_status_box_color):
			_jump_status_box_texture_rect.modulate = green_status_box_color
	else:
		if (_jump_status_box_texture_rect.modulate != red_status_box_color):
			_jump_status_box_texture_rect.modulate = red_status_box_color

func _update_dash_status_box():
	if (_dash_cooldown_timer <= 0.0 && _dash_count > 0):
		if (_dash_status_box_texture_rect.modulate != green_status_box_color):
			_dash_status_box_texture_rect.modulate = green_status_box_color
	else:
		if (_dash_status_box_texture_rect.modulate != red_status_box_color):
			_dash_status_box_texture_rect.modulate = red_status_box_color

func _update_health_bar():
	_health_bar_texture_progress.value = _health
#func _update_aim_vector():
#	var correction = max(sign(-_character_camera.transform.basis.z.y), _character_camera.transform.basis.z.z)
#	aim_vector = Vector3(-transform.basis.z.x + transform.basis.z.x * (1.0 - correction), -_character_camera.transform.basis.z.y, -transform.basis.z.z + transform.basis.z.z * (1.0 - correction))
#	aim_vector = -_character_camera.global_transform.basis.z

func _slash_with_combat_input():
	if(_input_combat_slash && _slash_cooldown_timer <= 0.0):
		_slash_initialisation_timer = SLASH_INITIALISATION
		_is_slashing = true
		_character_arm.play_animation()

func _slash():
	_slash_cooldown_timer = SLASH_COOLDOWN
	var slash = _slash.instance()
	slash.global_transform = _character_camera.global_transform
	get_tree().get_current_scene().add_child(slash)
	get_tree().get_current_scene().get_node("NetworkWorld").add_entity(slash.get_node("NetworkEntity"))

func _run_slash_cooldown_timer(delta):
	if (_slash_cooldown_timer > 0.0):
		_slash_cooldown_timer -= delta

func _run_slash_initialisation_timer(delta):
	if (_slash_initialisation_timer > 0.0):
		_slash_initialisation_timer -= delta
	else:
		if (_is_slashing):
			_slash()
			_is_slashing = false

func _on_collision_with_enemies(var collision_normal, var damage):
	if (typeof(collision_normal) == TYPE_VECTOR3):
		collision_normal.y = 0.0
		collision_normal = collision_normal.normalized()
		_add_to_velocity(collision_normal * 4.0)
	if (typeof(damage) == TYPE_REAL && _invlunerability_timer <= 0.0):
		_set_health(_health - damage)
		_character_audio_stream_player.set_stream(_hurt_sound)
		_character_audio_stream_player.play()
		if (_health <= 0.0):
			get_tree().reload_current_scene()
		_invlunerability_timer = INVULNERABILITY_COOLDOWN

func _collide_with_physicsbodys():
	var colliding_object : Object
	if(get_slide_count() > 0):
		for i in get_slide_count():
			colliding_object = get_slide_collision(i).collider
			if (colliding_object.is_in_group("enemy") && colliding_object.has_method("_start_idle_wander_cycle_timer_as_wander")):
				_on_collision_with_enemies(get_slide_collision(i).normal, colliding_object.get_damage())
				colcount += 1
				print("char_based_colcount:" + str(colcount))

func _set_health(var value):
	if (typeof(value) == TYPE_REAL):
		_health = value

func get_health():
	return _health

func _run_invulnerability_timer(delta):
	if (_invlunerability_timer > 0.0):
		_invlunerability_timer -= delta

func _jump():
	._jump()
	if (is_on_floor()):
		_character_audio_stream_player.set_stream(_jump_sound)
	else:
		_character_audio_stream_player.set_stream(_jump_air_sound)
	_character_audio_stream_player.play()
