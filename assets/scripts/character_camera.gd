##########################################################################
#   Copyright (C) 2021 Abdullah Çırağ
#   This file is part of 3D Project.
#   3D Project is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#   3D Project is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
##########################################################################
extends Camera
#MUST BE ATTACHED TO A CAMERA NODE WITH PLAYER PARENT
onready var character_kinematicbody = get_parent()
var sensitivity : Vector2
var mouse_relative : Vector2

func _ready():
	fov = Settings.fov
	sensitivity = Settings.sensitivity
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	Settings.connect("settings_changed", self, "_on_Settings_settings_changed")
	set_process(is_network_master())


func _input(event):
	if not is_network_master(): return
	if event is InputEventMouseMotion:
		mouse_relative = event.relative

func _process(delta):
	character_kinematicbody.rotate(Vector3.DOWN, sensitivity.x * mouse_relative.x / 600.0)
	rotate(Vector3.LEFT, sensitivity.y * mouse_relative.y / 600.0)
	set_rotation(Vector3(clamp(rotation.x, -PI / 2, PI / 2), rotation.y, rotation.z))
	mouse_relative = Vector2()

func _on_Settings_settings_changed():
	fov = Settings.fov
	sensitivity = Settings.sensitivity
