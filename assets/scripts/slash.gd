##########################################################################
#   Copyright (C) 2021 Abdullah Çırağ
#   This file is part of 3D Project.
#   3D Project is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#   3D Project is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
##########################################################################
extends Area

var _life_time = 0.3
var _life_time_timer = 0.0

var speed = 24.0
var _damage = 30.0

func _process(delta):
	_run_life_time_timer(delta)

func _physics_process(delta):
	transform.origin += -transform.basis.z * speed * delta

func _run_life_time_timer(delta):
	if(_life_time_timer < _life_time):
		_life_time_timer += delta
	else:
		queue_free()

func get_damage():
	return _damage

func _on_Slash_body_entered(var body):
	if (body.is_in_group("enemy")):
		if (body.has_method("_on_collision_with_player_projectile")):
			body._on_collision_with_player_projectile(_damage)
			queue_free()
