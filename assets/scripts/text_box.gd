##########################################################################
#   Copyright (C) 2021 Abdullah Çırağ
#   This file is part of 3D Project.
#   3D Project is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#   3D Project is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
##########################################################################
tool
extends Spatial

onready var text_box_viewport = get_node("TextBoxViewport")
onready var text_box_label = get_node("TextBoxViewport/TextBoxLabel")
onready var text_box_sprite_3d = get_node("TextBoxSprite3D")

export(String) var text
export(bool) var billboard

var viewport_texture : ViewportTexture

func _ready():
	text_box_label.set_text(text)
	text_box_viewport.update_size()
	text_box_sprite_3d.billboard = billboard

func _process(delta):
	if (Engine.editor_hint):
		text_box_viewport = get_node("TextBoxViewport")
		text_box_label = get_node("TextBoxViewport/TextBoxLabel")
		text_box_sprite_3d = get_node("TextBoxSprite3D")
		text_box_label.set_text(text)
		text_box_viewport.update_size()
		text_box_sprite_3d.billboard = billboard
