# 3D Project - A modular first-person video game.
## About
3D Project is an abstract, easy to build on first-person demonstration game  
with extensive movement mechanics. It is made to be used for development of  
other free (libre) games as a layer or simply for reference for video game
development; especially in Godot Engine.

## Exporting (Building)
Currently there are no pre-built binaries. You may export the game using the  
Godot Engine and the export templates, which you may find at

	https://godotengine.org/download

## License 
Copyright (C) 2021 Abdullah Çırağ

### Source Code and Non-media Files
3D Project is free software: you can redistribute it and/or modify  
it under the terms of the GNU General Public License as published by  
the Free Software Foundation, either version 3 of the License, or  
(at your option) any later version.  

This program is distributed in the hope that it will be useful,  
but WITHOUT ANY WARRANTY; without even the implied warranty of  
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License  
along with this program.  If not, see <https://www.gnu.org/licenses/>.  

### Artwork and Publication Files
Except where otherwise specified, all works of art and publications in this  
repository are licenced under CC BY-SA 4.0. See the following link  
	https://creativecommons.org/licenses/by-sa/4.0/

### Third-Party Works
3D Project is developed using Godot Engine licenced under the Expat License,  
also known as the MIT License. For more information, you may see

	https://godotengine.org/license

The files ambience.ogg, enemy_death.ogg and enemy_hurt.ogg found in the  
assets/sounds/ directory are distributed under CC0 with no copyright claim  
by the developer(s) of 3D Project.
