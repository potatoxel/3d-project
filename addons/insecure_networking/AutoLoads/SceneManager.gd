extends Node

signal on_scene_change

func change_scene(scene_path):
	get_tree().change_scene(scene_path)
	call_deferred("_change_scene_deffered")

func _change_scene_deffered():
	emit_signal("on_scene_change", get_tree().current_scene)
